# comedor-ciudad-caribia

Es uns**Sistema de Gestion y Control del Comedor Escolar**, la cual cumple con la
funcion de llevar un inventario, registrando los productos traidos con su respectivo
proveedor, fecha del suministro, la descripcion de la entrega hecha y la cantidad
que se ha suministrado. 

A su vez se encarga de asignar las tareas/asignaciones de los empleados mediante 
un horario, el cual el usuario podra observar. 
 
Se encarga del manejor del menú, creando los platos para la asignación del Menú.
*  Crear plato: Se ingresa el nombre del plato a crear, seguido de eso se selecciona el producto, en este casao seria el ingrediente.
*  Mostrar plato: Se muestra la lista de platos ya creada con el fin de no crear el mismo plato nuevamente.
*  Asignar: Mediante el plato seleccionado, muestra una ventana (Modal) en el que asigna el dia, y el momento de consumo del plato.


