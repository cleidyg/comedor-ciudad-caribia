CREATE DATABASE comedor;

USE comedor;


-- Producto


CREATE TABLE categoria(
  codcategoria     int(30) NOT NULL PRIMARY KEY auto_increment,
  nomcategoria     varchar(50) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE producto(
  codproduc      int(30) NOT NULL PRIMARY KEY auto_increment,
  nomproduc      varchar(50) NOT NULL,
  disponibilidad int(10) DEFAULT 0,
  codcategoria int(30) NOT NULL,
   foreign KEY (codcategoria) references categoria(codcategoria)
  )  ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE proveedor(
  codprove             int(30) NOT NULL PRIMARY KEY auto_increment,
  nomprove            varchar(50) NOT NULL,
  dirprove         varchar(50),
  pagweb          varchar(100),
  telefprove           varchar(30)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE provee(
  id_provee     int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  fechasumins   date NOT NULL,
  peso          int(10) NOT NULL,
  codproduc     int(30) NOT NULL,
  codprove      int(30) NOT NULL,
  descripcion  varchar(100) NOT NULL,
  foreign KEY (codproduc) references producto(codproduc),
  foreign KEY (codprove) references proveedor(codprove)
  )  ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE cargo(
  codcargo       int(30) NOT NULL PRIMARY KEY,
  cargo    varchar(50) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE empleados(
  idemp           int(30) NOT NULL PRIMARY KEY auto_increment,
  ci             varchar(30) NOT NULL,
  nomempleado    varchar(50) NOT NULL,
  apeempleado    varchar(50) NOT NULL,
  correo         varchar(50) NOT NULL,
  direccion      varchar(100) NOT NULL,
  telefono       varchar(50) NOT NULL,
  pre_seg         varchar(200) NOT NUll,
  res_seg         varchar(200) NOT NUll,
  nomuser        varchar(50) NOT NULL,
  password     varchar(50) NOT NULL,
  conf_password varchar(50) NOT NULL,
  cargo         varchar(50) NOT NULL,
  status         varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE horario(
  idhorario    int(30) NOT NULL PRIMARY KEY auto_increment,
  dia          varchar(50) NOT NULL,
  respon        varchar(50) NOT NULL,
  idemp        int(30) NOT NULL,    
   foreign KEY (idemp) references empleados(idemp)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE menu(
  codmenu      int(30) NOT NULL PRIMARY KEY auto_increment,
  nommenu      varchar(200) NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  CREATE TABLE recetas(
  codreceta      int(30) NOT NULL PRIMARY KEY auto_increment,
  codproduc      int(30) NOT NULL,
  codmenu         int(30) NOT NULL,
  foreign KEY (codproduc) references producto(codproduc),
  foreign KEY (codmenu) references menu(codmenu)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE asignacion(
  cod_asig     int(30) NOT NULL PRIMARY KEY auto_increment,
  dia      varchar(50) NOT NULL,
  consumo      varchar(15) NOT NULL,
  codmenu      int(30) NOT NULL,
  foreign KEY (codmenu) references menu(codmenu)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO empleados VALUES (1, 'V-123456', 'Juan', 'Perez', 'juan@gmail.com', 'Caracas', '0000-000-00-00', 'Cual es el nombre de mi mascota', 'Firulais', 'admin', 'admin', 'admin','administrador', 'activo');
INSERT INTO empleados VALUES (2, 'V-1111111', 'Johan', 'Cegarra', 'juan@gmail.com', 'Caracas', '0000-000-00-00', 'Cual es el nombre de mi mascota', 'Firulais', 'coc', 'coc', 'user','cocinero', 'activo');
INSERT INTO empleados VALUES (3, 'V-133333', 'Lucy', 'Hernandez', 'juan@gmail.com', 'Caracas', '0000-000-00-00', 'Cual es el nombre de mi mascota', 'Firulais', 'ayu', 'ayu', 'ayu','ayudante', 'activo');
INSERT INTO empleados VALUES (4, 'V-444444', 'Carlos', 'Perez', 'juan@gmail.com', 'Caracas', '0000-000-00-00', 'Cual es el nombre de mi mascota', 'Firulais', 'lim', 'lim', 'lim','limpieza', 'activo');
INSERT INTO empleados VALUES (5, 'V-5555555', 'Juana', 'Gonzales', 'juan@gmail.com', 'Caracas', '0000-000-00-00', 'Cual es el nombre de mi mascota', 'Firulais', 'coc', 'coc', 'coc','cocinera', 'activo');
INSERT INTO empleados VALUES (6, 'V-6666666', 'Maria', 'Valero', 'juan@gmail.com', 'Caracas', '0000-000-00-00', 'Cual es el nombre de mi mascota', 'Firulais', 'ayu', 'ayu', 'ayu','ayudante', 'activo');



INSERT INTO categoria  VALUES (1, 'Viveres');
INSERT INTO categoria  VALUES (2, 'Hortalizas');
INSERT INTO categoria  VALUES (3, 'Frutas');
INSERT INTO categoria  VALUES (4, 'Verduras');
INSERT INTO categoria  VALUES (5, 'Charcuteria');
INSERT INTO categoria  VALUES (6, 'Otras');

INSERT INTO producto (codproduc, nomproduc, disponibilidad, codcategoria) VALUES (1, 'Arroz', 12, 1);
INSERT INTO producto (codproduc, nomproduc, disponibilidad, codcategoria) VALUES (2, 'Carne', 12, 5);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (3,'Pollo', 12, 5);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (4,'Pasta', 32, 1);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (5,'Harina', 15, 1);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (6,'Queso', 13, 5);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (7,'Tomate', 15, 4);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (8,'Zanahoria', 16, 4);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (9,'Auyama', 17, 4);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (10,'Ocumo', 17, 4);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (11,'Papa', 15, 4);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (12,'Yuca', 16, 4);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (13,'Guayaba', 15, 3);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (14,'Cambur', 25, 3);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (15,'Lechosa', 26, 3);
INSERT INTO producto  (codproduc, nomproduc, disponibilidad, codcategoria)VALUES (16,'Peperoni', 16, 6);

INSERT INTO proveedor VALUES (1, 'CNAE', 'Esquinas salas, Parro. Altagracias', 'cnae.me.gob.ve', '0212-xxx-xx' );
INSERT INTO proveedor VALUES (2, 'MERCAL', 'La vega', 'www.mercal.gob.ve', '0212-xxx-xx');
INSERT INTO proveedor VALUES (3, 'D. Hortalizas', 'Ciudad caribia', 'www.xxx.com','0212-xxx-xx' );



CREATE TABLE empprodmenu(
  codtable int(30) NOT NULL PRIMARY KEY,
  codproduc        int(30),
  disponibilidad_old  int(10),
  disponibilidad int(10),  
  cambiado datetime,
  foreign KEY (codproduc) references producto(codproduc)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

delimiter //

CREATE TRIGGER empprodmenu BEFORE UPDATE ON producto
FOR EACH ROW 
BEGIN
INSERT INTO empprodmenu 
( codproduc, nomproduc, dispo_anterior, dispo_nueva, usuario,
cambiado)
VALUES 
(NEW.codproduc, NEW.nomproduc, OLD.disponibilidad, NEW.disponibilidad, CURRENT_USER(), NOW() );
END; //
delimiter ;

